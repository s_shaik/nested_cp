FROM node:10.22.0-alpine3.11
WORKDIR /app

RUN apk update && apk add python bash && rm -rf /var/cache/apk/*


RUN npm install
RUN npm install -g @vue/cli 
RUN npm install -g serve

